package com.example.diplomski;

public class Helper {
    public static String convertTimeSlotToString(int position) {
        switch (position)
        {
            case 0:
                return "8:00";
            case 1:
                return "8:35";
            case 2:
                return "9:10";
            case 3:
                return "9:45";
            case 4:
                return "10:20";
            case 5:
                return "10:55";
            case 6:
                return "11:30";
            case 7:
                return "12:05";
            case 8:
                return "12:40";
            case 9:
                return "13:15";
            case 10:
                return "13:50";
            case 11:
                return "14:25";
            case 12:
                return "15:00";
            case 13:
                return "15:35";
            case 14:
                return "16:10";
        }
        return "Nema slobodnih termina";
    }

    public static int convertTimeSlotToPosition(String termin) {
        switch (termin) {
            case "8:00":
                return 0;
            case "8:35":
                return 1;
            case "9:10":
                return 2;
            case "9:45":
                return 3;
            case "10:20":
                return 4;
            case "10:55":
                return 5;
            case "11:30":
                return 6;
            case "12:05":
                return 7;
            case "12:40":
                return 8;
            case "13:15":
                return 9;
            case "13:50":
                return 10;
            case "14:25":
                return 11;
            case "15:00":
                return 12;
            case "15:35":
                return 13;
            case "16:10":
                return 14;
            default:
                return -1; // Nije pronađena odgovarajuća pozicija
        }
    }
}
