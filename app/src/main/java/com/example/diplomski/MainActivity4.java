package com.example.diplomski;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;

public class MainActivity4 extends AppCompatActivity {
    LinkedList<TehnickiPregled> podaci;
    private String datum;
    private LinearLayout mainScrollView;
    private LayoutInflater inflater;
    private ConstraintLayout item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        mainScrollView = findViewById(R.id.verticalLayoutTehnickiPregledi);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        getAllTp();
    }


    public void setData() {
        mainScrollView.removeAllViews();

        for(TehnickiPregled tp : podaci) {
            item = (ConstraintLayout) inflater.inflate(R.layout.tehnicki_pregled_layout, null);

            TextView imeVlasnika = item.findViewById(R.id.imePrikaz);
            imeVlasnika.setText(tp.getIme_vlasnika());
            TextView markaModela = item.findViewById(R.id.markaModelPrikaz);
            markaModela.setText(tp.getMarka_Model());
            TextView tipVozila = item.findViewById(R.id.tipVozilaPrikaz);
            tipVozila.setText(tp.getTip_vozila());
            TextView datum = item.findViewById(R.id.datumPrikaz);
            datum.setText(tp.getDatum());
            TextView termin = item.findViewById(R.id.terminPrikaz);
            termin.setText(tp.getTermin());

            Button obrisi = item.findViewById(R.id.deleteTehnickiPregled);

            obrisi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteTehnickiPregled(tp);
                }
            });

            Button izmeni = item.findViewById(R.id.izmeniTehnickiPregled);

            izmeni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeActivityToUpdateTehnickiPregled(tp);
                }
            });

            mainScrollView.addView(item);
        }
    }

    public void getAllTp() {
        Api.getJson("http://10.0.2.2:5000/tehnickiPregledi", new ReadDataHandler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                try {
                    JSONArray jsonArray = new JSONArray(getJson());

                    podaci = TehnickiPregled.parseJsonArray(jsonArray);
                    setData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void deleteTehnickiPregled(TehnickiPregled tehnickiPregled) {
        Api.deleteJson("http://10.0.2.2:5000/deleteTehnickiPregled", tehnickiPregled, new ReadDataHandler());
        getAllTp();
        Toast.makeText(this, "Uspesno ste obrisali tehnicki pregled za automobil " + tehnickiPregled.getMarka_Model() + " vlasnika " + tehnickiPregled.getIme_vlasnika() + ". ", Toast.LENGTH_SHORT).show();
    }

    private void changeActivityToUpdateTehnickiPregled(TehnickiPregled tehnickiPregled) {
        Intent intent = new Intent(this, UpdateTehnickiPregledActivity.class);
        intent.putExtra("tehnickiPregled", tehnickiPregled);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}