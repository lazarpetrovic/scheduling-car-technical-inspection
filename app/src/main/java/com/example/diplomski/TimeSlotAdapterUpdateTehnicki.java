package com.example.diplomski;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

public class TimeSlotAdapterUpdateTehnicki extends RecyclerView.Adapter<TimeSlotAdapterUpdateTehnicki.MyViewHolder> {
    private final RecyclerViewInterface recyclerViewInterface;
    private Context context;
    private LinkedList<TehnickiPregled> tehnickiPregledi;
    private int selectedPosition = -1;
    private boolean isTimeSlotSelected = false;
    private TehnickiPregled tehnickiPregled;


    public TimeSlotAdapterUpdateTehnicki(Context context, LinkedList<TehnickiPregled> tehnickiPregledi, RecyclerViewInterface recyclerViewInterface) {
        this.context = context;
        this.tehnickiPregledi = tehnickiPregledi;
        this.recyclerViewInterface = recyclerViewInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.time_slot_layout, parent, false);

        return new MyViewHolder(itemView, recyclerViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        String vreme = Helper.convertTimeSlotToString(position);
        myViewHolder.prikazVremena.setText(new StringBuilder(vreme).toString());

        boolean zauzet = false;
        for(TehnickiPregled t : tehnickiPregledi) {
            if(t.getTermin().equals(vreme)) {
                if(tehnickiPregled.getTermin().equals(t.getTermin())){
                    continue;
                }
                else{
                    zauzet = true;
                    break;
                }
            }
        }

        if (zauzet) {
            myViewHolder.prikazStatusa.setText("Zauzet termin");
            myViewHolder.cardViewTimeSlot.setCardBackgroundColor(context.getResources().getColor(android.R.color.holo_red_light));
            myViewHolder.cardViewTimeSlot.setClickable(false);
        } else {
            myViewHolder.prikazStatusa.setText("Slobodan termin");
            if (position == selectedPosition && selectedPosition > -1) {
                myViewHolder.cardViewTimeSlot.setCardBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_light));
                isTimeSlotSelected = true;
            } else {
                myViewHolder.cardViewTimeSlot.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
            }
            //myViewHolder.cardViewTimeSlot.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
        }

        myViewHolder.prikazStatusa.setTextColor(context.getResources().getColor(android.R.color.black));
        myViewHolder.prikazVremena.setTextColor(context.getResources().getColor(android.R.color.black));
    }

    public void setTehnickiPregledZaIzmenu(TehnickiPregled tehnickiPregledZaIzmenu) {
        this.tehnickiPregled = tehnickiPregledZaIzmenu;
        notifyDataSetChanged();
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        isTimeSlotSelected = true;
        notifyItemChanged(position);
    }

    public void clearSelectedPosition() {
        int previousSelectedPosition = selectedPosition;
        selectedPosition = -1;
        isTimeSlotSelected = false;
        notifyItemChanged(previousSelectedPosition);
    }

    @Override
    public int getItemCount() {
        return 15;
    }

    public boolean isTimeSlotSelected() {
        return isTimeSlotSelected;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView prikazVremena, prikazStatusa;
        CardView cardViewTimeSlot;

        public MyViewHolder(@NonNull View itemView, RecyclerViewInterface recyclerViewInterface) {
            super(itemView);
            cardViewTimeSlot = (CardView) itemView.findViewById(R.id.cardSlotTime);
            prikazVremena = (TextView) itemView.findViewById(R.id.prikazVremena);
            prikazStatusa = (TextView) itemView.findViewById(R.id.prikazStatusa);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerViewInterface != null) {
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION) {
                            if (pos == selectedPosition) {
                                clearSelectedPosition();
                                recyclerViewInterface.onTimeSlotClear();
                            } else {
                                setSelectedPosition(pos);
                                Animation animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f,
                                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                animation.setDuration(200);
                                animation.setFillAfter(true);
                                cardViewTimeSlot.startAnimation(animation);
                                recyclerViewInterface.onItemClick(pos);
                            }
                        }
                    }
                }
            });

        }

        private void setSelectedPosition(int position) {
            int previousPosition = selectedPosition;
            selectedPosition = position;
            isTimeSlotSelected = true;
            notifyItemChanged(previousPosition);
            notifyItemChanged(position);
        }

        private void clearSelectedPosition() {
            int previousPosition = selectedPosition;
            selectedPosition = -1;
            isTimeSlotSelected = false;
            notifyItemChanged(previousPosition);
        }
    }
}
