package com.example.diplomski;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
//ovde treba dodati ucitavanje iz baze(zauzetih termina) u onZakazivanje i poslati preko intenta tu listu u mainActivity2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView img = findViewById(R.id.logoCarSystems);

        img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                LoginDialogActivity dialog = new LoginDialogActivity();
                dialog.show(getSupportFragmentManager(), "login dialog");

                return true;
            }
        });

        Button zakazivanje =findViewById(R.id.zakaziButton);

        zakazivanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivityOnZakazivanje();
            }
        });
    }

    private void changeActivityOnZakazivanje() {
        Intent i = new Intent(this, MainActivity2.class);
        startActivity(i);
        finish();
    }
}