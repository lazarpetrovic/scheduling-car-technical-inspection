package com.example.diplomski;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Api {
    public static void getJson(String url, final ReadDataHandler rdh) {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String odgovor = "";

                try {
                    URL url = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String red;

                    while( (red = reader.readLine()) != null) {
                        odgovor += red + "\n";
                    }
                    reader.close();
                    connection.disconnect();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return odgovor;
            }

            @Override
            protected void onPostExecute(String s) {
                rdh.setJson(s);
                rdh.sendEmptyMessage(0);
            }
        };
        task.execute(url);
    }

    public static void generateId(String url, final ReadDataHandler rdh) {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String odgovor = "";

                try {
                    URL url = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String red;

                    while( (red = reader.readLine()) != null) {
                        odgovor += red;
                    }
                    reader.close();
                    connection.disconnect();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return odgovor;
            }

            @Override
            protected void onPostExecute(String s) {
                rdh.setJson(s);
                rdh.sendEmptyMessage(0);
            }
        };
        task.execute(url);
    }

    public static void postDataJson(String url, TehnickiPregled objekat, final ReadDataHandler rdh) {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                //treba nam http konekcija
                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection con = (HttpURLConnection) link.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setDoInput(true);
                    con.setDoOutput(true);

                    Gson gson = new Gson();
                    String json = gson.toJson(objekat);

                    OutputStream outputStream = con.getOutputStream();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

                    //Ovako moze da radi ako prebacimo na JsonObject u MainActivity2 i dodajemo par po par dok ne popunimo json objekat
                    //outputStreamWriter.write(objekat.toString());
                    outputStreamWriter.write(json);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();

                    con.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    //cita se red po red iz br
                    String red;
                    while ((red = br.readLine()) != null) {
                        //Log.d("ovo je red iz while", red);
                        response += red + "\n";
                    }

                    br.close();
                    con.disconnect(); // ne mora se pisati - Android sam ubija konekciju

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                rdh.setJson(response);
                rdh.sendEmptyMessage(0);
            }
        };

        task.execute(url);
    }

    public static void deleteJson(String url, TehnickiPregled objekat, final ReadDataHandler rdh) {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                //treba nam http konekcija
                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection con = (HttpURLConnection) link.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setDoInput(true);
                    con.setDoOutput(true);

                    Gson gson = new Gson();
                    String json = gson.toJson(objekat);

                    OutputStream outputStream = con.getOutputStream();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

                    //Ovako moze da radi ako prebacimo na JsonObject u MainActivity2 i dodajemo par po par dok ne popunimo json objekat
                    //outputStreamWriter.write(objekat.toString());
                    outputStreamWriter.write(json);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();

                    con.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    //cita se red po red iz br
                    String red;
                    while ((red = br.readLine()) != null) {
                        //Log.d("ovo je red iz while", red);
                        response += red + "\n";
                    }

                    br.close();
                    con.disconnect(); // ne mora se pisati - Android sam ubija konekciju

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                rdh.setJson(response);
                rdh.sendEmptyMessage(0);
            }
        };

        task.execute(url);
    }

    public static void updateJson(String url, TehnickiPregled objekat, final ReadDataHandler rdh) {
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                //treba nam http konekcija
                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection con = (HttpURLConnection) link.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setDoInput(true);
                    con.setDoOutput(true);

                    Gson gson = new Gson();
                    String json = gson.toJson(objekat);

                    OutputStream outputStream = con.getOutputStream();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

                    //Ovako moze da radi ako prebacimo na JsonObject u MainActivity2 i dodajemo par po par dok ne popunimo json objekat
                    //outputStreamWriter.write(objekat.toString());
                    outputStreamWriter.write(json);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();

                    con.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    //cita se red po red iz br
                    String red;
                    while ((red = br.readLine()) != null) {
                        //Log.d("ovo je red iz while", red);
                        response += red + "\n";
                    }

                    br.close();
                    con.disconnect(); // ne mora se pisati - Android sam ubija konekciju

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                rdh.setJson(response);
                rdh.sendEmptyMessage(0);
            }
        };

        task.execute(url);
    }
}
