package com.example.diplomski;

public interface RecyclerViewInterface {
    void onItemClick(int position);

    void onTimeSlotClear();
}
