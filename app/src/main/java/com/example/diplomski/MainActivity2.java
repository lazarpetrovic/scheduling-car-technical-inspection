package com.example.diplomski;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, RecyclerViewInterface {

    private String[] tipoviVozila = {"Putnicko vozilo", "Teretno vozilo", "Motocikl", "Autobus"};
    private Context context;
    private String termin;
    private String datum;
    private TimeSlotAdapter timeSlotAdapter;
    //ArrayList<TehnickiPregled> lista;
    //Podaci o zakazanim tehnickim pregledima
    LinkedList<TehnickiPregled> podaci;
    TehnickiPregled noviTehnickiPregled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        noviTehnickiPregled = new TehnickiPregled();

        generateId();

        Button sledece = findViewById(R.id.sledeceButton);
        Button odabirDatuma = findViewById(R.id.odabirDatuma);

        Spinner tipVozilaSpinner = findViewById(R.id.tipSpinnerUnos);
        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tipoviVozila);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipVozilaSpinner.setAdapter(ad);

        odabirDatuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum = "";
                podaci = new LinkedList<>();
                getAllTehnickiPregledi();
                showDatePickerDialog();
            }
        });

        sledece.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText imeVlasnika = findViewById(R.id.imeVlasnikaUnos);
                EditText markaIModelVozila = findViewById(R.id.markaIModelUnos);

                String imeVlasnikaString = imeVlasnika.getText().toString();
                String markaIModelVozilaString = markaIModelVozila.getText().toString();

                String spinner = tipVozilaSpinner.getSelectedItem().toString();

                if(TextUtils.isEmpty(imeVlasnikaString)) {
                    imeVlasnika.setError("Morate uneti ime vlasnika vozila.");
                }
                else if(TextUtils.isEmpty(markaIModelVozilaString)) {
                    markaIModelVozila.setError("Morate uneti ime vlasnika vozila.");
                }
                else if(TextUtils.isEmpty(datum)) {
                    odabirDatuma.setError("Morate odabrati datum da bi zakazali tehnicki pregled.");
                }
                else if(!timeSlotAdapter.isTimeSlotSelected()) {
                    Toast.makeText(MainActivity2.this, "Morate kliknuti na neki termin da bi zavrsili zakazivanje tehnickog pregleda.", Toast.LENGTH_SHORT).show();
                }
                else{


                    noviTehnickiPregled.setIme_vlasnika(imeVlasnikaString);
                    noviTehnickiPregled.setMarka_Model(markaIModelVozilaString);
                    noviTehnickiPregled.setTip_vozila(spinner);
                    noviTehnickiPregled.setDatum(datum);
                    noviTehnickiPregled.setTermin(termin);


                    //Moze i na ovaj nacin da se dodaje u json file, probano je sa oba
                    /*JsonObject objekat = new JsonObject();
                    objekat.addProperty("ime_vlasnika", imeVlasnikaString);
                    objekat.addProperty("marka_model", markaIModelVozilaString);
                    objekat.addProperty("tip_vozila", spinner);
                    objekat.addProperty("datum", datum);
                    objekat.addProperty("termin", termin);*/

                    addTehnickiPregled(noviTehnickiPregled);

                    changeActivityToInfo(noviTehnickiPregled);
                }
            }
        });
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity2.this, this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        Long maksDatum = System.currentTimeMillis() + 5259487667L;
        Long trenutniDatum = System.currentTimeMillis() - 1000L;
        datePickerDialog.getDatePicker().setMinDate(trenutniDatum);
        datePickerDialog.getDatePicker().setMaxDate(maksDatum);

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int mesec = month + 1;
        datum = dayOfMonth + "/" + mesec + "/" + year;

        setupRecyclerView(datum);

        //kada se klikne na datum da tekst "Datum" promeni u oznaceni datum, to imam i kod zakazivanja i kod update tehnickog pregleda da bi se znalo na koji je kliknuto
        TextView datumTextView = findViewById(R.id.datumTextView);
        //datumTextView.setText(datum);
    }

    public void setupRecyclerView(String datum) {
        LinkedList<TehnickiPregled> podaciZaDatum = new LinkedList<>();

        for(TehnickiPregled t : podaci) {
            if(t.getDatum().equals(datum)) {
                podaciZaDatum.add(t);
            }
        }

        timeSlotAdapter = new TimeSlotAdapter(context, podaciZaDatum, this);

        RecyclerView recyclerView = findViewById(R.id.timeSlotRecycler);
        recyclerView.setAdapter(timeSlotAdapter);
        recyclerView.setHasFixedSize(true);

        GridLayoutManager grid = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(grid);
    }

    @Override
    public void onItemClick(int position) {
        termin = Helper.convertTimeSlotToString(position);
        //Toast.makeText(this, termin, Toast.LENGTH_SHORT);
    }

    @Override
    public void onTimeSlotClear() {
        termin = "";
    }

    public void getAllTehnickiPregledi() {
        Api.getJson("http://10.0.2.2:5000/tehnickiPregledi", new ReadDataHandler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                try {
                    JSONArray jsonArray = new JSONArray(getJson());

                    podaci = TehnickiPregled.parseJsonArray(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void addTehnickiPregled(TehnickiPregled objekat) {
        Api.postDataJson("http://10.0.2.2:5000/addTehnickiPregled", objekat, new ReadDataHandler());
    }

    public void generateId() {
        Api.generateId("http://10.0.2.2:5000/generateId", new ReadDataHandler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                String odgovor = getJson();

                noviTehnickiPregled.setId(odgovor);
            }
        });
    }

    private void changeActivityToInfo(TehnickiPregled tehnickiPregled) {
        Intent intent = new Intent(this, MainActivity3.class);
        intent.putExtra("tehnickiPregled", tehnickiPregled);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(MainActivity2.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}