package com.example.diplomski;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginDialogActivity extends AppCompatDialogFragment {
    private EditText korisnickoIme;
    private EditText lozinka;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_login_dialog, null);

        dialog.setView(view).setTitle("Login za administratora").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String kImeString = korisnickoIme.getText().toString();
                String lozinkaString = lozinka.getText().toString();

                if(kImeString.equals("admin") && lozinkaString.equals("admin")) {
                    Intent i = new Intent(getActivity(), MainActivity4.class);
                    startActivity(i);
                }
                else{
                    Toast.makeText(getActivity(), "Morate uneti ispravne podatke.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        korisnickoIme = view.findViewById(R.id.inputKorisnickoIme);
        lozinka = view.findViewById(R.id.inputLozinka);

        return dialog.create();
    }

    /*@Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (LoginDialogListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString());
        }
    }

    public interface LoginDialogListener {
        void login(String korisnickoIme, String lozinka);
    }*/
}