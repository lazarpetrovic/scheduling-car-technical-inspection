package com.example.diplomski;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Calendar;
import java.util.LinkedList;

public class UpdateTehnickiPregledActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, RecyclerViewInterface{

    private String[] tipoviVozila = {"Putnicko vozilo", "Teretno vozilo", "Motocikl", "Autobus"};
    private Context context;
    private String termin;
    private String datum;
    private TimeSlotAdapterUpdateTehnicki timeSlotAdapterIzmena;
    private TextView datumViewIzmena;
    //Podaci o zakazanim tehnickim pregledima
    LinkedList<TehnickiPregled> podaci;
    private TehnickiPregled tehnickiPregledZaIzmenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_tehnicki_pregled);

        datumViewIzmena = findViewById(R.id.datumViewIzmena);

        EditText imeVlasnikaZaIzmenu = findViewById(R.id.imeVlasnikaIzmena);
        EditText markaModelVozilaZaIzmenu = findViewById(R.id.markaModelIzmena);

        String imeVlasnikaIzmenaString = imeVlasnikaZaIzmenu.getText().toString();
        String markaModelVozilaIzmenaString = markaModelVozilaZaIzmenu.getText().toString();

        Spinner tipVozilaSpinnerIzmena = findViewById(R.id.spinnerIzmena);
        ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tipoviVozila);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipVozilaSpinnerIzmena.setAdapter(ad);

        String spinner = tipVozilaSpinnerIzmena.getSelectedItem().toString();

        TextView datumView = findViewById(R.id.datumTextView);

        tehnickiPregledZaIzmenu = new TehnickiPregled();

        Intent intent = getIntent();
        if(intent != null) {
            tehnickiPregledZaIzmenu = (TehnickiPregled) intent.getSerializableExtra("tehnickiPregled");

            imeVlasnikaZaIzmenu.setText(tehnickiPregledZaIzmenu.getIme_vlasnika());
            markaModelVozilaZaIzmenu.setText(tehnickiPregledZaIzmenu.getMarka_Model());
            tipVozilaSpinnerIzmena.setSelection(ad.getPosition(tehnickiPregledZaIzmenu.getTip_vozila()));
            datumViewIzmena.setText(tehnickiPregledZaIzmenu.getDatum());
            datum = tehnickiPregledZaIzmenu.getDatum();
            termin = tehnickiPregledZaIzmenu.getTermin();
        }


        Button odabirDatuma = findViewById(R.id.odabirDatumaIzmena);
        Button izmeniTehnicki = findViewById(R.id.izmeniButton);


        odabirDatuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum = "";
                //podaci = new LinkedList<>();
                //getAllTehnickiPregledi();
                showDatePickerDialog();
            }
        });

        izmeniTehnicki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tehnickiPregledZaIzmenu.setId(tehnickiPregledZaIzmenu.getId());
                tehnickiPregledZaIzmenu.setIme_vlasnika(imeVlasnikaZaIzmenu.getText().toString());
                tehnickiPregledZaIzmenu.setMarka_Model(markaModelVozilaZaIzmenu.getText().toString());
                tehnickiPregledZaIzmenu.setDatum(datum);
                tehnickiPregledZaIzmenu.setTermin(termin);
                sacuvajIzmenjeniTehnicki(tehnickiPregledZaIzmenu);
            }
        });

        getAllTehnickiPregledi();
    }

    private void sacuvajIzmenjeniTehnicki(TehnickiPregled tehnickiPregledZaIzmenu) {
        Log.d("ime", tehnickiPregledZaIzmenu.getIme_vlasnika());
        Log.d("markaModel", tehnickiPregledZaIzmenu.getMarka_Model());
        Log.d("Tip vozila", tehnickiPregledZaIzmenu.getTip_vozila());
        Log.d("datum", tehnickiPregledZaIzmenu.getDatum());
        Log.d("termin", tehnickiPregledZaIzmenu.getTermin());

        updateTehnickiPregledApi(tehnickiPregledZaIzmenu);
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateTehnickiPregledActivity.this, this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        Long maksDatum = System.currentTimeMillis() + 5259487667L;
        Long trenutniDatum = System.currentTimeMillis() - 1000L;
        datePickerDialog.getDatePicker().setMinDate(trenutniDatum);
        datePickerDialog.getDatePicker().setMaxDate(maksDatum);

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int mesec = month + 1;
        datum = dayOfMonth + "/" + mesec + "/" + year;

        datumViewIzmena.setText(datum);

        timeSlotAdapterIzmena.clearSelectedPosition();
        timeSlotAdapterIzmena.notifyDataSetChanged();
        
        setupRecyclerView();
    }

    public void setupRecyclerView() {
        LinkedList<TehnickiPregled> podaciZaDatum = new LinkedList<>();

        for(TehnickiPregled t : podaci) {
            if(t.getDatum().equals(datum)) {
                podaciZaDatum.add(t);
            }
        }

        timeSlotAdapterIzmena = new TimeSlotAdapterUpdateTehnicki(context, podaciZaDatum, this);
        timeSlotAdapterIzmena.setTehnickiPregledZaIzmenu(tehnickiPregledZaIzmenu);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewZaIzmenu);
        recyclerView.setAdapter(timeSlotAdapterIzmena);
        recyclerView.setHasFixedSize(true);

        GridLayoutManager grid = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(grid);

        int position = Helper.convertTimeSlotToPosition(tehnickiPregledZaIzmenu.getTermin());
        timeSlotAdapterIzmena.setSelectedPosition(position);
        timeSlotAdapterIzmena.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        termin = Helper.convertTimeSlotToString(position);
        //Toast.makeText(this, termin, Toast.LENGTH_SHORT);
    }

    @Override
    public void onTimeSlotClear() {
        termin = "";
    }

    public void getAllTehnickiPregledi() {
        Api.getJson("http://10.0.2.2:5000/tehnickiPregledi", new ReadDataHandler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                try {
                    JSONArray jsonArray = new JSONArray(getJson());

                    podaci = TehnickiPregled.parseJsonArray(jsonArray);
                    setupRecyclerView();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateTehnickiPregledApi(TehnickiPregled tehnickiPregled) {
        Api.updateJson("http://10.0.2.2:5000/updateTehnickiPregled", tehnickiPregled, new ReadDataHandler());
        changeActivityToMainActivity4();
    }

    private void changeActivityToMainActivity4() {
        Intent intent = new Intent(UpdateTehnickiPregledActivity.this, MainActivity4.class);

        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(UpdateTehnickiPregledActivity.this, MainActivity4.class);
        startActivity(i);
        finish();
    }

}
