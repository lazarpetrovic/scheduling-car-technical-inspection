package com.example.diplomski;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedList;

public class TehnickiPregled implements Serializable {
    String id;
    String ime_vlasnika;
    String marka_Model;
    String tip_vozila;
    String datum;
    String termin;

    public TehnickiPregled(String id, String ime_vlasnika, String marka_Model, String tip_vozila, String datum, String termin) {
        this.id = id;
        this.ime_vlasnika = ime_vlasnika;
        this.marka_Model = marka_Model;
        this.tip_vozila = tip_vozila;
        this.datum = datum;
        this.termin = termin;
    }

    public TehnickiPregled() {};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIme_vlasnika() {
        return ime_vlasnika;
    }

    public void setIme_vlasnika(String ime_vlasnika) {
        this.ime_vlasnika = ime_vlasnika;
    }

    public String getMarka_Model() {
        return marka_Model;
    }

    public void setMarka_Model(String marka_Model) {
        this.marka_Model = marka_Model;
    }

    public String getTip_vozila() {
        return tip_vozila;
    }

    public void setTip_vozila(String tip_vozila) {
        this.tip_vozila = tip_vozila;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getTermin() {
        return termin;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public static TehnickiPregled parseJsonObject(JSONObject object) {
        TehnickiPregled tehnickiPregled = new TehnickiPregled();

        try{
            if(object.has("ime_vlasnika")) {
                tehnickiPregled.setIme_vlasnika(object.getString("ime_vlasnika"));
            }
            if(object.has("marka_Model")) {
                tehnickiPregled.setMarka_Model(object.getString("marka_Model"));
            }
            if(object.has("tip_vozila")) {
                tehnickiPregled.setTip_vozila(object.getString("tip_vozila"));
            }
            if(object.has("datum")) {
                tehnickiPregled.setDatum(object.getString("datum"));
            }
            if(object.has("termin")) {
                tehnickiPregled.setTermin(object.getString("termin"));
            }
            if(object.has("id")) {
                tehnickiPregled.setId(object.getString("id"));
            }

        } catch (Exception e) {

        }
        return tehnickiPregled;
    }

    public static LinkedList<TehnickiPregled> parseJsonArray(JSONArray array) {
        LinkedList<TehnickiPregled> lista = new LinkedList<>();

        try {
            for(int i = 0; i < array.length(); i++) {
                TehnickiPregled t = parseJsonObject(array.getJSONObject(i));
                lista.add(t);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return lista;
    }
}
