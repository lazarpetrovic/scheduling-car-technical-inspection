package com.example.diplomski;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity {

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ConstraintLayout constraintLayout = findViewById(R.id.constraintLayoutID);

        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity3.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Toast.makeText(MainActivity3.this, "Ovaj prozor je informativnog tipa, iskljucice se za 10 sekundi ili klikom bilo gde na ekranu.", Toast.LENGTH_LONG).show();

        TextView vlasnik = findViewById(R.id.vlasnikTextView);
        TextView vozilo = findViewById(R.id.voziloTextView);
        TextView datum = findViewById(R.id.datumTextView);
        TextView termin = findViewById(R.id.terminTextView);

        Intent intent = getIntent();
        if(intent != null) {
            TehnickiPregled tp = (TehnickiPregled)intent.getSerializableExtra("tehnickiPregled");
            if(tp != null) {
                vlasnik.setText(tp.getIme_vlasnika());
                vozilo.setText(tp.getMarka_Model());
                datum.setText(tp.getDatum());
                termin.setText(tp.getTermin() + "h");
            }
        }

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity3.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };
        handler.postDelayed(runnable, 10000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(handler != null && runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }
}